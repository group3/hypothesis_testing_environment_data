# hypothesis_testing_environment_data

The objective of this program is only to output all the figures and
statistical analyses in the report and presentation.
This program can be run from the .ipynb file or main.py
Both 20240227_Report_DaiLe_group3.ipynb and main.py need to 
import the following files:

processing.py
utils.py
viz.py

If you like the project please star :))
## Usage

python main.py

## Support

If it doesn't run, please email: dle13@islander.tamucc.edu

## Roadmap

No further development. EOL

## Contributing

Financial contribution only

## Authors and acknowledgment

By the CrappyScientist

## License

MIT license

## Project status

Closed
