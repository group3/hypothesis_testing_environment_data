import glob
import itertools
import os
import string

import geopandas as gpd
import pandas as pd
import xarray as xr
from shapely.geometry import Point
from tqdm.auto import tqdm

legends = {
    "tp": ["Total Precipitation (m/day)", "Normalized Precipitation"],
    "t2m": ["Temperature (\u00B0C)", "Normalized Temperature"],
    "swvl1": [r"Soil Moisture ($m^3 m^{-3}$)", "Normalized Moisture"],
    "r": [r"Relative Humidity (\%)", "Normalized Humidity"],
    "latitude": ["Latitude (degree north)"],
    "longitude": ["Longitude (degree east)"],
    "time": ["Year"],
}


class Utils:
    CWD = os.getcwd()

    def __init__(self, data):
        self.data = data

    @staticmethod
    def concat_data_from_files(
            directory: str = CWD + "/dataset-ecv-for-climate-change",
            search: str = "_ea_tp_",
            extension: str = "grib",
            engine: str = "cfgrib",
    ) -> xr.core.dataset.Dataset:

        """
        Concatenate data from grib files on day dimension
        with xarray
        :param directory: str
        :param search: str
        :param extension: str
        :param engine: str
        :return: xr.core.dataset.Dataset
        """
        # Search for files with the specified pattern
        pattern = f"{directory}/**/*{search}*.{extension}"
        files = glob.glob(pattern, recursive=True)
        # Ensure there are files to process
        if not files:
            raise FileNotFoundError(f"No files matching '{pattern}' found.")

        datasets = []
        for idx, file_path in tqdm(
                enumerate(files),
                desc="Loading files",
                total=len(files),
        ):
            try:
                # Use 'with' statement to ensure the file is properly closed after being loaded
                with (xr.open_dataset(
                        file_path,
                        engine=engine,
                        chunks={'time': 100},
                        backend_kwargs={'indexpath': ''},
                ) as ds):
                    datasets.append(ds)
            except Exception as e:
                print(f"Error loading {file_path}: {e}")

        # Concatenate all datasets along the 'time' dimension
        if datasets:
            final_dataset = xr.concat(datasets, dim='time').sortby("time")
            print("DONE!")
            return final_dataset
        else:
            raise ValueError("No datasets were loaded, unable to concatenate.")

    @staticmethod
    def xr_to_sdf(dfs: list[xr]) -> list[pd.DataFrame]:
        new_sdf = []
        for idx, df in tqdm(enumerate(dfs), desc="Creating spatial dataframes", total=len(dfs)):
            sdf = df[df.attrs["name"]].mean(dim='time').to_dataframe().reset_index().fillna(0)
            try:
                sdf = sdf.drop(["number", "step", "surface"], axis=1, )
            except KeyError as e:
                sdf = sdf.drop(["number", "step"], axis=1, )
            sdf.name = df.attrs["name"]
            new_sdf.append(sdf)
        print("DONE!")
        return new_sdf

    @staticmethod
    def sdf_to_gdf(sdfs: list[pd]) -> list[gpd]:
        new_sdf = []
        for idx, df in tqdm(
                enumerate(sdfs),
                desc="Converting to geodataframes",
                total=len(sdfs),
        ):
            gdf = gpd.GeoDataFrame(
                df[df.name],
                geometry=
                [Point(xy) for xy in zip(
                    df["longitude"],
                    df["latitude"],
                )
                 ]
            )
            new_sdf.append(gdf)
        print("DONE")
        return new_sdf

    @staticmethod
    def xr_to_tdf(dfs: list[xr]) -> list[pd.DataFrame]:
        new_tdf = []
        for idx, df in tqdm(
                enumerate(dfs),
                desc="Creating temporal dataframes",
                total=len(dfs),
        ):
            var = df.attrs["name"]
            tdf = df[var].mean(
                dim=['latitude', 'longitude'],
            ).to_dataframe().reset_index()[[var, "time"]]
            new_tdf.append(tdf)
        print("DONE!")
        return new_tdf

    @staticmethod
    def generate_labels(n):
        def iter_all_strings():
            size = 1
            while True:
                for s in itertools.product(string.ascii_uppercase, repeat=size):
                    yield "".join(s)
                size += 1

        labels = []
        for s in iter_all_strings():
            labels.append(s)
            if len(labels) == n:
                break
        return labels

    @staticmethod
    def alphabet_label_generator():
        for size in itertools.count(1):
            for s in itertools.product(string.ascii_uppercase, repeat=size):
                yield "".join(s)

    @staticmethod
    def simple_alphabet_label_generator():
        from itertools import product, count
        import string

        # Single letters A-Z
        yield from string.ascii_uppercase

        # Double letters AA, AB, ..., ZZ, then triple, etc.
        for size in count(2):  # Starting from 2 letters
            for letters in product(string.ascii_uppercase, repeat=size):
                yield ''.join(letters)
