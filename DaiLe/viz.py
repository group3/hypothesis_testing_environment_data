import geopandas as gpd
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.graph_objects as go
import xarray as xr
from matplotlib.colors import Normalize
from plotly.offline import plot
from scipy.stats import gaussian_kde
from tqdm.auto import tqdm

# plt.style.use(["science"])

MARKERS = ['o', 's', '^', 'v', '*', '+', 'x', 'D']


class plots:
    def __init__(
            self,
            data,
    ):
        self.data = data

    @staticmethod
    def calculate_figure_size(
            nrows: int,
            ncols: int,
            width_per_subplot: float = 4,
            height_per_subplot: float = 2,
            extra_width: float = 5,
            extra_height: float = 3,
    ) -> tuple[float, float]:
        """
        Calculate figure size dynamically based on the number of subplots.

        Parameters:
        - rows: Number of subplot rows.
        - cols: Number of subplot columns.
        - width_per_subplot: Desired width per subplot in inches.
        - height_per_subplot: Desired height per subplot in inches.
        - extra_width: Extra width to account for figure padding and colorbars in inches.
        - extra_height: Extra height to account for titles, labels, etc., in inches.

        Returns:
        - figsize: A tuple (width, height) representing the figure size in inches.
        """
        width = ncols * width_per_subplot + extra_width
        height = nrows * height_per_subplot + extra_height
        return width, height

    @staticmethod
    def time_grid(
            df: xr.core.dataset.Dataset,
            # var: str,
            auto_open: bool = False,
            save: bool = False,
            filename: str = None,
    ) -> None:

        """
        Plots gridded data over a time period
        :param df: pd.DataFrame
        :param var: str,
        :param auto_open: bool
        :param save: bool
        :param filename: str
        :return: None
        """
        figures = []
        for time_step in tqdm(np.unique(df['time'].values), desc="Time steps"):
            filtered_df = df[df['time'] == time_step]
            fig = go.Choropleth(
                z=filtered_df.astype(float),
                colorscale='Viridis',
            )
            figures.append(fig)

        steps = []
        for i, time_step in tqdm(enumerate(df['time'].unique()), desc="Slider steps"):
            step = dict(
                method='restyle',
                args=['visible', [False] * len(df['time'].unique())],
                label=str(time_step),
            )
            step['args'][1][i] = True
            steps.append(step)

        sliders = [dict(
            active=0,
            currentvalue={"prefix": "Time: "},
            steps=steps,
        )]

        layout = go.Layout(
            sliders=sliders,
            geo=dict(
                projection_type='natural earth'
            ),
        )
        fig = go.Figure(data=figures, layout=layout)
        fig.show()
        if save:
            if auto_open:
                plot(fig, filename=filename, auto_open=True)
            else:
                plot(fig, filename=filename, auto_open=False)

    @staticmethod
    def spatiotemporal(
            ax: mpl.axes.Axes,
            df: xr.core.dataset.Dataset,
            legends: dict[str],
            label: str,
            reduced_ax: str,
            save: bool = False,
            transform: mpl.colors = Normalize,
            normalized: bool = False,
    ) -> [plt.colorbar, mpl.figure.Figure]:

        """
        Create single spatiotemporal plot
        :param normalized: bool =False
        :param legends: dict[list[str]
        :param ax: mpl.axes.Axes
        :param transform: mpl.colors
        :param df: xr.core.dataset.Dataset
        :param label: str
        :param reduced_ax: str
        :param figsize: tuple[int,int]
        :param save: bool
        :return: cbar: plt.colorbar
        :return fig: plt.figure
        """
        vars = ["time", "latitude", "longitude"]
        vmin = df[df.attrs["name"]].mean(dim=reduced_ax).min().values
        vmax = df[df.attrs["name"]].mean(dim=reduced_ax).max().values
        norm = transform(vmin=vmin, vmax=vmax)
        mesh = df[df.attrs["name"]].mean(dim=reduced_ax).plot.pcolormesh(
            ax=ax,
            cmap='RdBu_r',
            add_colorbar=False,
            norm=norm,
        )
        temp_list = [el for el in vars if el != reduced_ax]
        cbar = plt.colorbar(mesh, ax=ax)
        ax.set_title(f'{legends[reduced_ax][0]}', fontsize=14, fontweight='bold')
        cbar.set_label(label[0], fontsize=14, fontweight='bold')
        ax.set_ylabel(legends[temp_list[0]][1], fontsize=14, fontweight='bold')
        ax.set_xlabel(legends[temp_list[1]][1], fontsize=14, fontweight='bold')
        if save:
            plt.savefig(f'{label}.png')
        return cbar

    @classmethod
    def collate_spatiotemporal(
            cls,
            legends: dict[str, list[str]],
            dfs: list[xr.core.dataset.Dataset],
            fig_title: str = None,
            save: bool = False,
            normalized: bool = False,
    ) -> None:
        """
        Collate single spatial temporal plot into panels
        :param normalized: bool=False,
        :param legends:  dict[list[str]]
        :param dfs: list[xr.core.dataset.Dataset]
        :param figsize: tuple
        :param save: bool
        :param fig_title: str
        :return: None
        """

        vars = ["time", "latitude", "longitude"]
        if not dfs:
            print("Warning: 'dfs' is empty. No subplots will be created.")
            return
        nrows = len(dfs)
        ncols = len(vars)
        figsize = cls.calculate_figure_size(nrows, ncols)
        fig, axs = plt.subplots(
            nrows=nrows,
            ncols=ncols,
            figsize=figsize,
            dpi=300,
            constrained_layout=True,
        )
        # plt.figure(layout='constrained')
        axs = axs if dfs else [axs]
        for row_idx, df in tqdm(enumerate(dfs), desc="Loop over datasets", total=len(dfs)):
            for col_idx, reduced_ax in tqdm(enumerate(vars), desc="Loop over reduced variables", total=len(vars)):
                ax = axs[row_idx][col_idx] if nrows > 1 else axs[col_idx]
                transform = mpl.colors.LogNorm if df.attrs["name"] == "tp" else mpl.colors.Normalize
                label = legends[df.attrs["name"]]

                cbar = cls.spatiotemporal(
                    ax=ax,
                    df=df,
                    label=label,
                    legends=legends,
                    reduced_ax=reduced_ax,
                    save=False,
                    transform=transform,
                    normalized=normalized,
                )
                '''ax.text(
                    -0.1, 1.1, next(utils.Utils.alphabet_label_generator()),
                    transform=ax.transAxes,
                    fontsize=18, fontweight='bold',
                    va='top', ha='left',
                )'''
                if row_idx == 0:
                    ax.set_title(legends[reduced_ax][0], fontsize=16, fontweight='bold')
                else:
                    ax.set_title("")
                    ax.tick_params(
                        axis='x',
                        bottom=True,
                        top=False,
                        labelbottom=True,
                        labeltop=False,
                    )
                if col_idx < ncols - 1:
                    cbar.set_label("")
                if row_idx < nrows - 1:
                    ax.set_xlabel("")
        fig.subplots_adjust(
            left=0.05,
            hspace=0.1,
            top=0.1,
            bottom=0.05,
            right=0.05,
        )
        fig.tight_layout()
        # fig.show()
        if save:
            plt.savefig(f'{fig_title}.png')

    @classmethod
    def time_series(
            cls,
            legends: dict[list[str]],
            dfs: pd.DataFrame,
            colormap: str = "Set3",
    ) -> None:
        """
        Plot time series from dataframes
        :param legends: dict[list[str
        :param df: pd.DataFrame,
        :param colormap:
        :return: None
        """
        width, height = cls.calculate_figure_size(
            nrows=1,
            ncols=1,
        )
        cmap = plt.cm.get_cmap(colormap)
        ncolors = len(dfs.columns)
        colors = [cmap(i / ncolors) for i in range(ncolors)]

        fig, ax = plt.subplots(
            figsize=(width, height),
            dpi=300,
        )
        plt.figure(layout='constrained')
        line_labels = []
        for idx, df in tqdm(enumerate(dfs.items()), desc="Plotting time series", total=len(dfs.columns)):
            ax.plot(
                df,
                color=colors[idx],
                label=dfs.columns[idx],
            )
            ax.tick_params(
                axis='both',
                # labelsize=14,
            )
            ax.set_xlabel(
                'Year',
                # fontsize=18,
            )
            line_labels.append(dfs.columns[idx])
            # fontsize=18,

        # ax1.set_ylim(65, 80)
        ax.set_title('Time series of Normalized Precipitation, Humidity, Temperature, Soil Moisture')
        fig.tight_layout(pad=0.5)
        ax.legend(loc='best')
        fig.show()

    @classmethod
    def histogram(
            cls,
            legends: dict[str, list[str]],
            dfs: pd.DataFrame,
            # colormap: str = "RdBu_r",
    ) -> None:
        """
            Plot histogram from dataframes
            :param legends: dict[list[str
            :param df: pd.DataFrame,
            :param colormap:
            :return: None
            """
        width, height = cls.calculate_figure_size(
            nrows=len(dfs.columns),
            ncols=1,
            width_per_subplot=2,
            height_per_subplot=0.1,
            extra_width=7,
            # extra_height=1,
        )
        fig, axs = plt.subplots(
            figsize=(width, height),
            nrows=1,
            ncols=len(dfs.columns),
            dpi=300,
            sharex=True,
            sharey=True,
        )
        # plt.figure(layout='constrained')
        for idx, col in tqdm(enumerate(dfs), desc="Plotting histogram", total=len(dfs.columns)):
            density_m = gaussian_kde(dfs[col])
            density_m.covariance_factor = lambda: .25
            density_m._compute_covariance()
            # density_y = gaussian_kde(dfs[1][col])
            # density_y.covariance_factor = lambda: .25
            # density_y._compute_covariance()
            x_vals_m = np.linspace(min(dfs[col]), max(dfs[col]), 1000)
            # x_vals_y = np.linspace(min(dfs[1][col]), max(dfs[1][col]), 1000)
            nbins_m = int(np.sqrt(len(dfs[col])))
            # nbins_y = int(np.sqrt(len(dfs[1][col])))
            axs[idx].hist(dfs[col], bins=nbins_m, color="green", density=True)
            axs[idx].plot(x_vals_m, density_m(x_vals_m), color="red", linewidth=2)
            axs[idx].tick_params(axis='both')
            axs[idx].set_xlabel(col, fontsize=14)
            #axs[idx].set_ylabel("Density", fontsize=14)
            """axs[idx].text(
                -0.1, 1.1, next(utils.Utils.alphabet_label_generator()),
                transform=axs[idx].transAxes,
                fontsize=14, fontweight='bold',
                va='top', ha='left',
            )"""
            # axs[idx, 1].hist(dfs[1][col], bins=nbins_y, color="green", density=True)
            # axs[idx, 1].plot(x_vals_y, density_y(x_vals_y), color="red", linewidth=2)
            if idx == 0:
                axs[idx].set_ylabel("Density", fontsize=14)
            """if idx == 0:
                axs[idx].set_title("Monthly Data", fontsize=14)"""
                # axs[idx, 1].set_title("Yearly Data")
        fig.tight_layout(pad=0.5)

    @classmethod
    def scatter(
            cls,
            df: pd.DataFrame,
            colormap: str = "Accent",
    ) -> None:
        """
        Scatter plot of linear relationship between two variables
        :param colormap:
        :param df:
        :return:
        """
        ncols = 3
        nrows = 1
        width, height = cls.calculate_figure_size(
            ncols=ncols,
            nrows=nrows,
            width_per_subplot=2.5,
            height_per_subplot=1.5,
            extra_width=2,
            extra_height=2,
        )
        fig, axs = plt.subplots(
            nrows=nrows, ncols=ncols,
            figsize=(width, height),
            dpi=300
        )
        cmap = plt.cm.get_cmap(colormap)
        ncolors = len(df.columns)
        colors = [cmap(i / ncolors) for i in range(ncolors)]
        axs[0].scatter(
            df["Humidity"],
            df["Precipitation"],
            label="Precipitation",
            color=colors[0],
            # marker=MARKERS[0],
            s=42,
        )
        axs[0].set_xlabel("Humidity", fontsize=16)
        axs[0].set_ylabel("Precipitation", fontsize=16)
        axs[1].scatter(
            df["Humidity"],
            df["Temperature"],
            label="Temperature",
            color=colors[1],
            # marker=MARKERS[1],,
            s=42,
        )
        axs[1].set_xlabel("Humidity", fontsize=16)
        axs[1].set_ylabel("Temperature", fontsize=16)
        axs[2].scatter(
            df["Soil Moisture"],
            df["Precipitation"],
            color=colors[2],
            # marker=MARKERS[2],,
            s=42,
        )
        axs[2].set_xlabel("Soil Moisture", fontsize=16)
        axs[2].set_ylabel("Precipitation", fontsize=16)
        fig.tight_layout(pad=0.3)

    @classmethod
    def time_series_all_vars(
            cls,
            df: pd.DataFrame,
            colormap: str = "Set3",
    ) -> None:
        """
        Plot time series for all normalized variables
        :param df:
        :param colormap:
        :return:
        """
        cmap = plt.cm.get_cmap(colormap)
        ncolors = len(df.columns)
        colors = [cmap(i / ncolors) for i in range(ncolors)]
        # Plot time series for normalized variables
        plt.figure(dpi=300)
        for idx, col in enumerate(df.columns):
            plt.plot(df.index, df[col], label=col, color=colors[idx], marker=MARKERS[idx])
        # plt.title("Time series of Normalized Precipitation, \nHumidity, Temperature, Soil Moisture", fontsize=10)
        plt.xlabel("Year", fontsize=16, fontweight="bold")
        plt.ylabel("", fontsize=16, fontweight="bold")
        plt.xticks(fontsize=8)
        plt.yticks(fontsize=8)
        plt.legend(fontsize=8, loc='lower right')
        plt.tight_layout(pad=0.5)

    @classmethod
    def lagged_vars(
            cls,
            lagged_r: np.ndarray,
            lagged_s: np.ndarray,
            tgdf: gpd,
            pgdf: gpd,
            colormap: str = "Dark2",
    ) -> None:
        """

        :param colormap:
        :param lagged_r:
        :param lagged_s:
        :param tgdf:
        :param pgdf:
        :return: None
        """
        ncols = 3
        nrows = 1
        width, height = cls.calculate_figure_size(
            ncols=ncols,
            nrows=nrows,
            width_per_subplot=2,
            height_per_subplot=1,
            extra_width=2,
            extra_height=2,
        )
        fig, axs = plt.subplots(
            nrows=nrows, ncols=ncols, figsize=(width, height), dpi=300
        )
        cmap = plt.cm.get_cmap(colormap)
        colors = [cmap(i / 4) for i in range(4)]
        axs[0].plot(lagged_r, pgdf["tp"], color=colors[0])
        axs[0].set_xlabel("Lagged Humidity", fontsize=14, fontweight="bold")
        axs[0].set_ylabel("Precipitation", fontsize=14, fontweight="bold")
        axs[1].plot(lagged_r, tgdf["t2m"], color=colors[1])
        axs[1].set_xlabel("Lagged Humidity", fontsize=14, fontweight="bold")
        axs[1].set_ylabel("Temperature", fontsize=14, fontweight="bold")
        axs[2].plot(lagged_s, pgdf["tp"], color=colors[2])
        axs[2].set_xlabel("Lagged Soil Moisture", fontsize=14, fontweight="bold")
        axs[2].set_ylabel("Precipitation", fontsize=14, fontweight="bold")
        axs[2].legend(loc="lower left", fontsize=14)
        plt.tight_layout(pad=0.3)
