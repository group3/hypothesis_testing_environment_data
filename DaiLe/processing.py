import pandas as pd
from tqdm.auto import tqdm


class Scaler:
    def __init__(self, data):
        self.data = data

    @staticmethod
    def minmax_scaler(data) -> [list, float, float]:
        """
        Scale the data to a range between 0 and 1.

        Parameters:
        - data (list or numpy array): The input data to scale.

        Returns:
        - scaled_data: The data scaled to the range [0, 1].
        - min_val: The minimum value in the original data.
        - max_val: The maximum value in the original data.
        """
        min_val = min(data)
        max_val = max(data)
        range_val = max_val - min_val
        scaled_data = [(x - min_val) / range_val for x in data]
        return scaled_data, min_val, max_val

    @staticmethod
    def inverse_minmax_scaler(
            scaled_data,
            min_val: float,
            max_val: float,
    ) -> list:
        """
        Inverse the scaling operation to return to the original data scale.

        Parameters:
        - scaled_data (list or numpy array): The scaled data to inverse scale.
        - min_val: The minimum value in the original data.
        - max_val: The maximum value in the original data.

        Returns:
        - original_data: The data after inversing the scaling operation.
        """
        original_data = [x * (max_val - min_val) + min_val for x in scaled_data]
        return original_data

    @classmethod
    def mm_scale(cls, data: list[pd]) -> list:
        scaled_data = []
        for idx, df in tqdm(
                enumerate(data),
                desc=" Min Max Scaling",
                total=len(data),
        ):
            scaled, minval, maxval = cls.minmax_scaler(df)
            scaled_data.append(scaled)
        return scaled_data
