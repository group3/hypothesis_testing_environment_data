if __name__ == '__main__':
    import pandas as pd
    import matplotlib.pyplot as plt
    from tqdm.auto import tqdm
    import utils
    from scipy.stats import kendalltau
    import viz
    import statsmodels.api as sm
    from libpysal.weights import Queen, lag_spatial
    from scipy.stats import pearsonr
    import importlib
    import processing
    import utils

    if __name__ == "__main__":
        # plt.style.use(['science'])
        plt.rcParams.update({
            "text.usetex": True,
            "font.family": "Times New Roman",
            'font.weight': "bold",
            'axes.labelweight': "bold",
            'axes.titleweight': "bold",

        })
        importlib.reload(viz)
        importlib.reload(utils)
        importlib.reload(processing)
        # Convert data variables to something more readable
        legends = {
            "tp": ["Total Precipitation (m/day)", "Precipitation"],
            "t2m": ["Temperature (\u00B0C)", "Temperature"],
            "swvl1": [r"Soil Moisture ($m^3 m^{-3}$)", "Soil Moisture"],
            "r": [r"Relative Humidity (\%)", "Humidity"],
            "latitude": ["Averaged Over Latitude", "Latitude (degree north)"],
            "longitude": ["Averaged Over Longitude", "Longitude (degree east)"],
            "time": ["Averaged Over Time", "Year"],
        }

        print("Concatenating files to create xarray datasets for each data")
        dfs = []
        for i in tqdm(["tp", "2t", "swvl1", "r2"], leave=True, position=0):
            arr = utils.Utils.concat_data_from_files(search=f'_{i}_')
            if i == "2t":
                arr.attrs["name"] = "t2m"
            elif i == "r2":
                arr.attrs["name"] = "r"
            else:
                arr.attrs["name"] = i
            dfs.append(arr)
        tp, t2m, swvl1, r2 = [df for df in dfs]
        # Convert Kalvin degree to Celsius degree
        t2m["t2m"] = t2m["t2m"] - 273.15
        print("DONE!")

        print("Producing figures")
        try:
            viz.plots(dfs).collate_spatiotemporal(
                dfs=dfs,
                legends=legends,
                fig_title="Figure1_combined",
                save=False,
                normalized=False,
            )
        except RuntimeError as e:
            if "layout engine" in str(e):
                pass
            else:
                raise
        print("DONE!")

        print("Converting dataframes")
        psdf, tsdf, ssdf, rsdf = [df for df in utils.Utils.xr_to_sdf(dfs)]
        pgdf, tgdf, sgdf, rgdf = [df for df in utils.Utils.sdf_to_gdf([psdf, tsdf, ssdf, rsdf])]
        ptdf, ttdf, stdf, rtdf = [df for df in utils.Utils.xr_to_tdf(dfs)]
        print("DONE!")

        p_tnorm, t_tnorm, s_tnorm, r_tnorm = processing.Scaler.mm_scale([
            ptdf["tp"],
            ttdf["t2m"],
            stdf["swvl1"],
            rtdf["r"],
        ])

        time_index = pd.date_range(start='1989-12-31', periods=372, freq='ME')

        normalized_df = pd.DataFrame(
            columns=["Precipitation", "Temperature", "Soil Moisture", "Humidity"],
            index=time_index,
        )
        normalized_df["Precipitation"] = p_tnorm
        normalized_df["Temperature"] = t_tnorm
        normalized_df["Soil Moisture"] = s_tnorm
        normalized_df["Humidity"] = r_tnorm
        # Take averages over 12 months of a year
        normalized_df_yearly = normalized_df.resample('YE').mean().iloc[1:, :]
        normalized_df = normalized_df.reset_index()

        print("Plot time series for all variables")
        viz.plots.time_series_all_vars(normalized_df_yearly)
        print("Plot histograms for distributions of all variables")
        viz.plots.histogram(legends=legends, dfs=normalized_df.iloc[:, 1:])
        print("""Plot scatter plots to examine linearity between 
              humidity vs precipitation, humidity vs temperature, soil moisture vs precipitation
              """)
        viz.plots.scatter(df=normalized_df_yearly)

        print("Running statistical tests on temporal correlation")
        X_r = sm.add_constant(normalized_df_yearly["Humidity"])
        r_t2m = sm.OLS(normalized_df_yearly["Temperature"], X_r)
        r_t2m_result = r_t2m.fit()
        r_tp = sm.OLS(normalized_df_yearly["Precipitation"], X_r)
        r_tp_result = r_tp.fit()
        X_tp = sm.add_constant(normalized_df_yearly["Precipitation"])
        s_tp = sm.OLS(normalized_df_yearly["Soil Moisture"], X_tp)
        s_tp_result = s_tp.fit()
        corr_t2m_r2, pp_t2m_r2 = pearsonr(normalized_df_yearly["Humidity"], normalized_df_yearly["Temperature"])
        corr_tp_r2, pp_tp_r2 = pearsonr(normalized_df_yearly["Humidity"], normalized_df_yearly["Precipitation"])
        corr_tp_swvl1, pp_tp_swvl1 = pearsonr(normalized_df_yearly["Precipitation"],
                                              normalized_df_yearly["Soil Moisture"])
        kdtau = pd.DataFrame(columns=["X", "Y", "tau", "p_tau"])
        tau_t2m_r2, p_t2m_r2 = kendalltau(normalized_df_yearly["Humidity"], normalized_df_yearly["Temperature"])
        tau_tp_r2, p_tp_r2 = kendalltau(normalized_df_yearly["Humidity"], normalized_df_yearly["Precipitation"])
        tau_tp_swvl1, p_tp_swvl1 = kendalltau(normalized_df_yearly["Precipitation"],
                                              normalized_df_yearly["Soil Moisture"])
        stats_df = pd.DataFrame(columns=["X", "Y", "t_coeff", "p_of_t", "r", "p_of_uncorr", "tau", "p_of_tau"])
        stats_df["X"] = ["Humidity", "Humidity", "Precipitation"]
        stats_df["Y"] = ["Temperature", "Precipitation", "Soil Moisture"]
        stats_df["t_coeff"] = [r_t2m_result.tvalues.iloc[1], r_tp_result.tvalues.iloc[1], s_tp_result.tvalues.iloc[1]]
        stats_df["p_of_t"] = [r_t2m_result.pvalues.iloc[1], r_tp_result.pvalues.iloc[1], s_tp_result.pvalues.iloc[1]]
        stats_df["r"] = [corr_t2m_r2, corr_tp_r2, corr_tp_swvl1]
        stats_df["p_of_uncorr"] = [pp_t2m_r2, pp_tp_r2, pp_tp_swvl1]
        stats_df["tau"] = [tau_t2m_r2, tau_tp_r2, tau_tp_swvl1]
        stats_df["p_of_tau"] = [p_t2m_r2, p_tp_r2, p_tp_swvl1]
        print("Results of statistical tests")
        print(stats_df)

        print("Running statistical tests on spatial cross correlation")
        p_snorm, t_snorm, s_snorm, r_snorm = processing.Scaler.mm_scale([
            pgdf["tp"],
            tgdf["t2m"],
            sgdf["swvl1"],
            rgdf["r"],
        ])
        rgdf["r"] = r_snorm
        pgdf["tp"] = p_snorm
        tgdf["t2m"] = t_snorm
        sgdf["swvl1"] = s_snorm

        W_rgdf = Queen.from_dataframe(rgdf, use_index=False)
        W_rgdf.transform = 'r'
        W_pgdf = Queen.from_dataframe(pgdf, use_index=False)
        W_pgdf.transform = 'r'
        W_tgdf = Queen.from_dataframe(tgdf, use_index=False)
        W_tgdf.transform = 'r'
        W_sgdf = Queen.from_dataframe(sgdf, use_index=False)
        W_sgdf.transform = 'r'

        lagged_r = lag_spatial(W_rgdf, rgdf['r'])
        lagged_s = lag_spatial(W_sgdf, sgdf["swvl1"])

        print("Plot referenced variable as a function of lagged spatial variable")
        viz.plots.lagged_vars(
            lagged_r=lagged_r,
            lagged_s=lagged_s,
            tgdf=tgdf,
            pgdf=pgdf,
        )

        tau_r_t2m, p_r_t2m = kendalltau(lagged_r, tgdf['t2m'])
        tau_r_tp, p_r_tp = kendalltau(lagged_r, pgdf["tp"])
        tau_s_tp, p_s_tp = kendalltau(lagged_s, pgdf["tp"])
        corr_r_t2m, pp_r_t2m = pearsonr(lagged_r, tgdf['t2m'])
        corr_r_tp, pp_r_tp = pearsonr(lagged_r, pgdf["tp"])
        corr_s_tp, pp_s_tp = pearsonr(lagged_s, pgdf["tp"])

        spatial_df = pd.DataFrame(columns=["Lagged_X", "Y", "r", "p_of_uncorr", "tau", "p_of_tau"])
        spatial_df["Lagged_X"] = ["Humidity", "Humidity", "Precipitation"]
        spatial_df["Y"] = ["Temperature", "Precipitation", "Soil Moisture"]
        spatial_df["r"] = [corr_r_t2m, corr_r_tp, corr_s_tp]
        spatial_df["p_of_uncorr"] = [pp_r_t2m, pp_r_tp, pp_s_tp]
        spatial_df["tau"] = [tau_r_t2m, tau_r_tp, tau_s_tp]
        spatial_df["p_of_tau"] = [p_r_t2m, p_r_tp, p_s_tp]

        print("Results of statistical tests on spatial cross-correlation")
        print(spatial_df)
        print("ALL DONE!")
