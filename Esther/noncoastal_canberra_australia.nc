CDF       
      lon       time     lat             Conventions       CF-1.0     title         ?CPC Merged Analysis of Precipitation (excludes NCEP Reanalysis)    platform      Analyses   source        -ftp ftp.cpc.ncep.noaa.gov precip/cmap/monthly      dataset_title         $CPC Merged Analysis of Precipitation   documentation         9https://www.esrl.noaa.gov/psd/data/gridded/data.cmap.html      date_modified         26 Feb 2019    _NCProperties         "version=2,netcdf=4.6.3,hdf5=1.10.5     
References        4https://www.psl.noaa.gov/data/gridded/data.cmap.html   version       V2402      history       update 02/2024 V2402   data_modified         
2024-02-12     History       �Translated to CF-1.0 Conventions by Netcdf-Java CDM (CFGridCoverageWriter)
Original Dataset = precip.mon.mean.nc; Translation Date = 2024-02-25T01:23:51.697Z      geospatial_lat_min        �B�        geospatial_lat_max        �A�        geospatial_lon_min        @bp        geospatial_lon_max        @b�              lon                 units         degrees_east   	long_name         	Longitude      actual_range      ?�  C�`    standard_name         	longitude      axis      X           	<   time               units          hours since 1800-01-01 00:00:0.0   	long_name         Time   delta_t       0000-01-00 00:00:00    
avg_period        0000-01-00 00:00:00    standard_name         time   axis      T      actual_range      A7�0    A=�       calendar      	gregorian        �  	@   lat                units         degrees_north      actual_range      B�� ±�    	long_name         Latitude   standard_name         latitude   axis      Y           (   precip                        	long_name         %Average Monthly Rate of Precipitation      units         mm/day     
add_offset               scale_factor      ?�     missing_value         ��     	precision               least_significant_digit             var_desc      Precipitation      dataset       -CPC Merged Analysis of Precipitation Standard      
level_desc        Surface    	statistic         Mean   parent_stat       Mean   valid_range           D�     actual_range          D�G�   coordinates       time lat lon         t  ,C� A7�0    A7�    A7��    A7��    A7�p    A7�X    A8(    A8    A8�    A8
�    A8�    A8�    A8h    A8P    A8    A8�    A8�    A8!�    A8$x    A8'`    A8*H    A8-    A80     A82�    A85�    A88�    A8;@    A8>(    A8@�    A8C�    A8F�    A8I�    A8L�    A8OP    A8R8    A8U    A8W�    A8Z�    A8]x    A8``    A8c0    A8f    A8h�    A8k�    A8n�    A8q�    A8tp    A8w@    A8z(    A8}    A8�    A8��    A8�h    A8�P    A8�     A8�    A8��    A8��    A8��    A8�x    A8�`    A8�H    A8�     A8��    A8��    A8��    A8�p    A8�X    A8�@    A8�    A8��    A8��    A8��    A8��    A8�8    A8�     A8��    A8��    A8Ϩ    A8Ґ    A8�x    A8�H    A8�0    A8�     A8��    A8��    A8�p    A8�X    A8�(    A8�    A8��    A8��    A8��    A8��    A8�h    A9 8    A9     A9    A9�    A9�    A9`    A9H    A9    A9     A9�    A9�    A9�    A9"p    A9%X    A9(@    A9*�    A9-�    A90�    A93�    A96h    A99P    A9<8    A9?    A9A�    A9D�    A9G�    A9J�    A9M0    A9P    A9R�    A9U�    A9X�    A9[�    A9^p    A9a@    A9d(    A9f�    A9i�    A9l�    A9oh    A9rP    A9u     A9x    A9z�    A9}�    A9��    A9�x    A9�`    A9�0    A9�    A9�     A9��    A9��    A9�X    A9�@    A9�    A9��    A9��    A9��    A9��    A9�h    A9�P    A9�8    A9��    A9��    A9��    A9��    A9�`    A9�H    A9�0    A9�     A9��    A9͸    A9Р    A9ӈ    A9�(    A9�    A9��    A9��    A9�    A9�    A9�h    A9�8    A9�     A9��    A9��    A9��    A9�`    A9�H    A9�    A:     A:�    A:�    A:	�    A:p    A:X    A:(    A:    A:�    A:�    A:�    A: P    A:#8    A:&    A:(�    A:+�    A:.�    A:1�    A:4`    A:7H    A::0    A:<�    A:?�    A:B�    A:E�    A:HX    A:K@    A:N(    A:P�    A:S�    A:V�    A:Y�    A:\�    A:_     A:b    A:d�    A:g�    A:j�    A:mx    A:p`    A:s0    A:v    A:x�    A:{�    A:~�    A:�X    A:�@    A:�    A:��    A:��    A:��    A:��    A:�h    A:�P    A:�     A:�    A:��    A:��    A:�x    A:�H    A:�0    A:�     A:��    A:��    A:��    A:��    A:�X    A:�@    A:�(    A:��    A:��    A:˘    A:΀    A:�P    A:�8    A:�     A:��    A:��    A:ߨ    A:�    A:�x    A:�    A:�     A:��    A:�    A:�    A:�p    A:�X    A:�(    A:�    A;�    A;�    A;�    A;
P    A;8    A;    A;�    A;�    A;�    A;�    A;`    A;!H    A;$    A;'     A;)�    A;,�    A;/p    A;2@    A;5(    A;7�    A;:�    A;=�    A;@�    A;C�    A;FP    A;I8    A;L     A;N�    A;Q�    A;T�    A;Wx    A;ZH    A;]0    A;`    A;b�    A;e�    A;h�    A;k�    A;np    A;q    A;s�    A;v�    A;y�    A;|�    A;h    A;�P    A;�     A;�    A;��    A;��    A;��    A;�H    A;�0    A;�     A;��    A;��    A;��    A;��    A;�X    A;�@    A;�    A;��    A;��    A;��    A;�h    A;�8    A;�     A;��    A;��    A;��    A;ɐ    A;�x    A;�H    A;�0    A;�    A;��    A;ڸ    A;݈    A;�p    A;�@    A;�(    A;�    A;��    A;��    A;�    A;�    A;�h    A;�    A;��    A;��    A<�    A<x    A<`    A<H    A<    A<     A<�    A<�    A<�    A<@    A<(    A<!�    A<$�    A<'�    A<*�    A<-�    A<0P    A<38    A<6    A<8�    A<;�    A<>x    A<A`    A<D0    A<G    A<I�    A<L�    A<O�    A<R�    A<Up    A<X@    A<[(    A<^    A<`�    A<c�    A<f�    A<ih    A<l8    A<o     A<r    A<t�    A<w�    A<z�    A<}x    A<�`    A<�     A<��    A<��    A<��    A<�p    A<�X    A<�@    A<�    A<��    A<��    A<��    A<��    A<�8    A<�     A<��    A<��    A<��    A<��    A<�x    A<�H    A<�0    A<�     A<��    A<��    A<�p    A<�X    A<�(    A<�    A<��    A<��    A<ذ    A<ۀ    A<�h    A<�8    A<�     A<�    A<��    A<�    A<�x    A<�`    A<�0    A<�    A<�     A<��    A= �    A=�    A=p    A=	X    A=�    A=�    A=�    A=�    A=h    A=P    A=8    A=     A="�    A=%�    A=(�    A=+�    A=.0    A=1    A=3�    A=6�    A=9�    A=<�    A=?p    A=B@    A=E(    A=G�    A=J�    A=M�    A=Ph    A=SP    A=V     A=Y    A=[�    A=^�    A=a�    A=dx    A=g`    A=j0    A=m    A=p     A=r�    A=u�    A=xp    A={X    A=~(    A=�    A=��    A=��    A=��    A=��    A=�h    A=�P    A=��    A=��    A=��    A=��    A=�`    A=�H    A=�0    A=�     A=��    A=��    A=��    A=��    A=�(    A=�    A=��    A=��    A=    A=ŀ    A=�h    A=�8    A=�     A=��    A=��    A=��    A=�`    A=�H    A=�    A=�     A=��    A=�    A=�    A=�p    A=�X    A=�(    A=�    �  ?+�>aG�@z=q@��?��
?xQ�?c�
@{@%@\)?���>k�@>{?J=q?z�H?5@�\@ ��@��?�@�@$z�?�@L(�?��@J=q?s33?B�\@�Q�@�  @Z�H@w
=?���?�p�@'
=@Q�?�(�>Ǯ@l(�?�ff?���?�?s33?�R@�R?p��>L��?��?��
?ٙ�@N{@G�@�@@\)@;�@P  @��\@]p�@�@��H@,��@z�@,(�?aG�?�
=@��
@�p�@��?�  ?�ff?��R?�R?��\@#33@&ff@@   ?��
@u�@ ��@��\@��@W�@Dz�>�p�>�z�?���?�  ?�Q�@(Q�?�(�?�G�@@]p�?�33@,(�@
=q?�G�?E�?��?�
=@�?ٙ�?aG�?�ff?˅@��?Y��?�G�?�@~�R@c�
?�@/\)?��R@p�?:�H@��H@���?�?�  @�(�@��H@  @<(�@:=q?�p�?��?���@<(�?�?�  @�z�?fff@���@2�\?h��?�@%@�@*�H?��?�ff@'
=?��?�z�?Y��?��@��\@�33?��@ff?G�?G�@0��@�\@J=q?�\)?��?�33?�(�?�Q�@	��@J=q@   @�{@l��@\)?�33@N{?&ff?���?��@AG�?�33@�G�@�@�?�?5@��R@
=@��?aG�?�33?��>�(�?��
?\@33?�=q@�Q�?�p�?!G�?�(�@��@�p�@{�?�=q@(�@�G�@�G�@�z�@9��@=p�@�\@�
@,(�@XQ�@���@XQ�@���@:�H@xQ�@�H@{?��@
=>L��@
=@`  ?���?�
=@I��?�(�?��?���?У�?�{>���@?���@��@1�@�Q�@/\)@	��@A�?�\)@n{?B�\@'�?�z�?�33@�\?�Q�?��R@��@-p�@�\@P��?�{?���@;�?��@@  @G�?�p�@5@l��@HQ�@���?��\?��H@mp�@  ?��?J=q@ ��@Q�@W�@�@P  @\)?�=q?�G�@�(�?�G�@p�?���@*=q?���?��@?aG�?���?xQ�>�\)@�?��
?�?�@Q�?�=q@X��?�
=@1�@(��@.�R?�Q�?fff>�=q?��?��@(�?���@{@*�H@$z�@l��@b�\?�Q�@@��?�33?�G�>�ff@��@B�\@/\)@u@>{@2�\?��@\)?��?s33?��@(�@�
@�?�Q�?У�>�ff?�
=?c�
?u@��?�
=?�ff@�@�Q�@��?^�R?�G�?s33@y��@<(�@G�@7�?��?�Q�?p��?���@   @��?�p�?B�\@x��@!G�?�\)?h��?h��@5?k�@��?�  @   @2�\@1�?�33@
�H?��@���@��?���@Q�?�z�@	��@QG�@�@�{@�z�@�
=?���@�\)@`��?��@��?�
=@1G�@�@�\@   @�(�@ ��@��@���@�ff@��@G�@=p�?�p�?�
=?�33?�\)@
=q?\@�
@b�\?��R?��R@�R@�33@�@��@'
=?��@(��?�G�?�z�?�=q@S33@tz�?�
=@h��?�33?���?�ff?�Q�?��@��@l��?�p�>���@�33@�\?�G�@'
=@4z�?xQ�?�G�@J=q?�  @�Q�>B�\?�z�?���@C�
@�z�@\��?˅@��@2�\?�z�@��?��R?Y��@4z�@�
@G�>�=q?�
=@��?^�R?�z�@%@u@333?��?Q�?=p�?��?޸R?n{?�33?5?���@O\)@!G�?���?�
=@p��>�Q�@%�?���?��?Tz�?���?^�R@>Ǯ@  @s�
@!�@Vff?�ff?���@O\)@G
=?�(�@��H?�
=@+�@7�@C�
@�p�>�G�@Z=q@Q�@�R@�@'
=@%@��@j�H@�  @I��@]p�@'�?��
?��?�33@7
=@@  @��@��H@+�@B�\?��
@0  @\)?�
=?���?!G�?u?=p�@.{@�33@�(�@\��