CDF       
      lon       time     lat             Conventions       CF-1.0     title         ?CPC Merged Analysis of Precipitation (excludes NCEP Reanalysis)    platform      Analyses   source        -ftp ftp.cpc.ncep.noaa.gov precip/cmap/monthly      dataset_title         $CPC Merged Analysis of Precipitation   documentation         9https://www.esrl.noaa.gov/psd/data/gridded/data.cmap.html      date_modified         26 Feb 2019    _NCProperties         "version=2,netcdf=4.6.3,hdf5=1.10.5     
References        4https://www.psl.noaa.gov/data/gridded/data.cmap.html   version       V2402      history       update 02/2024 V2402   data_modified         
2024-02-12     History       �Translated to CF-1.0 Conventions by Netcdf-Java CDM (CFGridCoverageWriter)
Original Dataset = precip.mon.mean.nc; Translation Date = 2024-02-25T01:13:01.111Z      geospatial_lat_min        @B�        geospatial_lat_max        @D         geospatial_lon_min        �_@        geospatial_lon_max        �^               lon                 units         degrees_east   	long_name         	Longitude      actual_range      ?�  C�`    standard_name         	longitude      axis      X           	<   time               units          hours since 1800-01-01 00:00:0.0   	long_name         Time   delta_t       0000-01-00 00:00:00    
avg_period        0000-01-00 00:00:00    standard_name         time   axis      T      actual_range      A7�0    A=�       calendar      	gregorian        �  	D   lat                units         degrees_north      actual_range      B�� ±�    	long_name         Latitude   standard_name         latitude   axis      Y           ,   precip                        	long_name         %Average Monthly Rate of Precipitation      units         mm/day     
add_offset               scale_factor      ?�     missing_value         ��     	precision               least_significant_digit             var_desc      Precipitation      dataset       -CPC Merged Analysis of Precipitation Standard      
level_desc        Surface    	statistic         Mean   parent_stat       Mean   valid_range           D�     actual_range          D�G�   coordinates       time lat lon         �  0Cl@ Cn� A7�0    A7�    A7��    A7��    A7�p    A7�X    A8(    A8    A8�    A8
�    A8�    A8�    A8h    A8P    A8    A8�    A8�    A8!�    A8$x    A8'`    A8*H    A8-    A80     A82�    A85�    A88�    A8;@    A8>(    A8@�    A8C�    A8F�    A8I�    A8L�    A8OP    A8R8    A8U    A8W�    A8Z�    A8]x    A8``    A8c0    A8f    A8h�    A8k�    A8n�    A8q�    A8tp    A8w@    A8z(    A8}    A8�    A8��    A8�h    A8�P    A8�     A8�    A8��    A8��    A8��    A8�x    A8�`    A8�H    A8�     A8��    A8��    A8��    A8�p    A8�X    A8�@    A8�    A8��    A8��    A8��    A8��    A8�8    A8�     A8��    A8��    A8Ϩ    A8Ґ    A8�x    A8�H    A8�0    A8�     A8��    A8��    A8�p    A8�X    A8�(    A8�    A8��    A8��    A8��    A8��    A8�h    A9 8    A9     A9    A9�    A9�    A9`    A9H    A9    A9     A9�    A9�    A9�    A9"p    A9%X    A9(@    A9*�    A9-�    A90�    A93�    A96h    A99P    A9<8    A9?    A9A�    A9D�    A9G�    A9J�    A9M0    A9P    A9R�    A9U�    A9X�    A9[�    A9^p    A9a@    A9d(    A9f�    A9i�    A9l�    A9oh    A9rP    A9u     A9x    A9z�    A9}�    A9��    A9�x    A9�`    A9�0    A9�    A9�     A9��    A9��    A9�X    A9�@    A9�    A9��    A9��    A9��    A9��    A9�h    A9�P    A9�8    A9��    A9��    A9��    A9��    A9�`    A9�H    A9�0    A9�     A9��    A9͸    A9Р    A9ӈ    A9�(    A9�    A9��    A9��    A9�    A9�    A9�h    A9�8    A9�     A9��    A9��    A9��    A9�`    A9�H    A9�    A:     A:�    A:�    A:	�    A:p    A:X    A:(    A:    A:�    A:�    A:�    A: P    A:#8    A:&    A:(�    A:+�    A:.�    A:1�    A:4`    A:7H    A::0    A:<�    A:?�    A:B�    A:E�    A:HX    A:K@    A:N(    A:P�    A:S�    A:V�    A:Y�    A:\�    A:_     A:b    A:d�    A:g�    A:j�    A:mx    A:p`    A:s0    A:v    A:x�    A:{�    A:~�    A:�X    A:�@    A:�    A:��    A:��    A:��    A:��    A:�h    A:�P    A:�     A:�    A:��    A:��    A:�x    A:�H    A:�0    A:�     A:��    A:��    A:��    A:��    A:�X    A:�@    A:�(    A:��    A:��    A:˘    A:΀    A:�P    A:�8    A:�     A:��    A:��    A:ߨ    A:�    A:�x    A:�    A:�     A:��    A:�    A:�    A:�p    A:�X    A:�(    A:�    A;�    A;�    A;�    A;
P    A;8    A;    A;�    A;�    A;�    A;�    A;`    A;!H    A;$    A;'     A;)�    A;,�    A;/p    A;2@    A;5(    A;7�    A;:�    A;=�    A;@�    A;C�    A;FP    A;I8    A;L     A;N�    A;Q�    A;T�    A;Wx    A;ZH    A;]0    A;`    A;b�    A;e�    A;h�    A;k�    A;np    A;q    A;s�    A;v�    A;y�    A;|�    A;h    A;�P    A;�     A;�    A;��    A;��    A;��    A;�H    A;�0    A;�     A;��    A;��    A;��    A;��    A;�X    A;�@    A;�    A;��    A;��    A;��    A;�h    A;�8    A;�     A;��    A;��    A;��    A;ɐ    A;�x    A;�H    A;�0    A;�    A;��    A;ڸ    A;݈    A;�p    A;�@    A;�(    A;�    A;��    A;��    A;�    A;�    A;�h    A;�    A;��    A;��    A<�    A<x    A<`    A<H    A<    A<     A<�    A<�    A<�    A<@    A<(    A<!�    A<$�    A<'�    A<*�    A<-�    A<0P    A<38    A<6    A<8�    A<;�    A<>x    A<A`    A<D0    A<G    A<I�    A<L�    A<O�    A<R�    A<Up    A<X@    A<[(    A<^    A<`�    A<c�    A<f�    A<ih    A<l8    A<o     A<r    A<t�    A<w�    A<z�    A<}x    A<�`    A<�     A<��    A<��    A<��    A<�p    A<�X    A<�@    A<�    A<��    A<��    A<��    A<��    A<�8    A<�     A<��    A<��    A<��    A<��    A<�x    A<�H    A<�0    A<�     A<��    A<��    A<�p    A<�X    A<�(    A<�    A<��    A<��    A<ذ    A<ۀ    A<�h    A<�8    A<�     A<�    A<��    A<�    A<�x    A<�`    A<�0    A<�    A<�     A<��    A= �    A=�    A=p    A=	X    A=�    A=�    A=�    A=�    A=h    A=P    A=8    A=     A="�    A=%�    A=(�    A=+�    A=.0    A=1    A=3�    A=6�    A=9�    A=<�    A=?p    A=B@    A=E(    A=G�    A=J�    A=M�    A=Ph    A=SP    A=V     A=Y    A=[�    A=^�    A=a�    A=dx    A=g`    A=j0    A=m    A=p     A=r�    A=u�    A=xp    A={X    A=~(    A=�    A=��    A=��    A=��    A=��    A=�h    A=�P    A=��    A=��    A=��    A=��    A=�`    A=�H    A=�0    A=�     A=��    A=��    A=��    A=��    A=�(    A=�    A=��    A=��    A=    A=ŀ    A=�h    A=�8    A=�     A=��    A=��    A=��    A=�`    A=�H    A=�    A=�     A=��    A=�    A=�    A=�p    A=�X    A=�(    A=�    B  @��@��
A�@�z�@4z�@]p�?�
=?�  ?p��?z�H<#�
=�\)=�Q�>��=���>B�\>�=q=�\)@e@AG�@�G�@\)@�z�@�(�@��Ap�A33A�@�@!G�@{?�\?��?�>�
=>��R<�>��    =#�
=��
=�?z�H?
=q?G�?Tz�@��@*=q@�G�@��
@G
=@�@c�
@�  ?+�?z�H?s33?��\=#�
<#�
<�<#�
<#�
<#�
?J=q?�@mp�@dz�AA\)@��H@�R@�{@�p�@�=q@��
@�Q�@�G�@�G�@���    >#�
    >.{<�=�G�<��
=�Q�?��@\)@mp�@�z�@���@ҏ\@�ff@��@�  @ҏ\A$��A��ABffA�H@���@vff?!G�?#�
>\)>���>L��=u?��?�>�Q�?�p�?B�\?��
A*�HA)�A2{AQ�?   >���@G�@Z�H@��@�
?�ff?��R>�G�>�33>�  ?!G�<#�
>aG�>#�
>W
=>8Q�>W
=@\)@	��A$  @���@�
?��R?O\)?E�@%�@:=q@��H@�
=>u>�z�=�G�=#�
=L��>B�\=#�
=�Q�=u=�Q�?��\?�p�?���?���@|(�@�{@AG�@,(�@S�
@p�@�@�\)@W
=@\)?:�H?#�
?n{>�z�<��
<�<#�
=��
    <#�
?aG�>�>��>8Q�>�33>�?�\)?W
=@Y��@33@R�\@4z�@K�@#�
>k�>\)=��
>���<�=�G�=L��<#�
<#�
            ?J=q?B�\@�?��
@��@%�@�R@��>���>�Q�>k�>8Q�?��H?�=q?n{>�?&ff>��    <#�
<#�
<#�
<#�
    >.{=�\)@{�@%�@*=q@G�?��R?@  ?s33?z�H@���@���?Y��>���>�p�>��>���>�
=<#�
    =���>W
=@��?�@Q�?��R?��\?n{=u<#�
@w�@N�R?��R?�p�?�  ?fff>Ǯ>�G�@{?��=�Q�<��
>��=�>k�=�\)>8Q�=�Q�>\>k�?:�H>��H?�  ?�z�>�ff>�\)@9��@(�@���@�Q�?��>�{?(�>�p�>.{>�z�=���=L��=�\)=�Q�<#�
=u?:�H?Tz�?
=>�(�?�{?�z�?�=q?��@��H@��H@333@��?n{?5=�\)<�?+�>�ff<��
=L��<�=#�
=#�
    ?�
=?���>�
=>#�
@�{@�z�@���@˅@��@�@'
=@33?�\)?:�H?�G�?z�H?B�\?=p�<�    >8Q�=#�
        ?8Q�?��?У�?�(�@   ?��
@�?���@��H@6ff?   >u?��\?J=q?�Q�?�  =��
<#�
<#�
<#�
        =L��=��
>��>�
=@�p�@"�\@Mp�@�@ҏ\@�33>�>�=q@ٙ�@���?�(�?h��?J=q?���?@  ?5<#�
<��
=�Q�=�G�=��
<�=u<#�
>k�=#�
@�(�@��@��@P��@��@�{?��H?��?�
=?��
@�?�ff=u=�Q�<�<��
    <#�
>W
==�\)?���?(��?��H?�  @�(�@��
@��@��\>��H>�  ?B�\>aG�?333>�  >���>aG�?�>Ǯ        >��
>.{>��>W
=?��>��@�G�@P  @)��@.�R@�G�@�=qA8  A+�@E�@9��?��H@+�@j�H@dz�?5?\)<�<#�
        =�?��?��?fff@��@��?�Q�?��\@�
@o\)@��R@�  @(��@ ��?�G�?���>��>��>u>#�
<#�
=�\)=�G�=�<�<�>�(�?(�@)��@?(��>�@��H@�G�@ÅA�\?�{?��?�
=?��H?\(�?���>��
>�\)>W
==�G�>�=�Q�>��R>��R@ff@��?G�?B�\?\(�?�ff@P��@;�@�{@��\?�
=?�{?�\)?�Q�=��
=#�
=�G�=�G�<#�
<#�
<�<#�
>\)>u?�=q?��@�z�@S�
@�z�@�{@e@   ?�?���@33@33?z�?��>�?E�=u        <��
    <�<#�
<#�
<�<#�
@p�@!�A	��@�p�@\��?��?�?�ff@�
?�p�@S33?��?^�R?Y��<#�
<��
=#�
<�=���>��H=��
<#�
=�\)=�Q�@=q?�G�@�@�ff@G�?�=q@�p�@�z�?�  ?E�?0��>��>���>�z�<#�
=#�
        <#�
<#�
=L��=�Q�@
=@:�H@G�?��@�@p  @s33@AG�@K�@>�R@vff@p�?�{?���@Mp�?��?fff>�ff    <�<��
=�\)=#�
>\)?333>��@Q�?�(�A2=qAQ�@���@8Q�@\(�@   @�=q@�\)@�33@q�?   ?�R=��
=#�
<#�
<��
<#�
=L��<#�
<#�
>���>�{@:=q?У�@�  @8��        @��H@�          ?���?n{>�{>.{<�=��
>���=�<#�
    =�\)>��?���?�      >��@333@�@�33@�
=?=p�?���                <��
<��
        <#�
<#�
=L��            >��?�?��
?���?�=q?��\    ?(�@��@`��?^�R?�p�    >��
?�G�?�>�{>u<��
<��
<#�
<#�
=L��>�@33@�R=#�
>\)?��H?���@�G�@��@z=q@\)?�{?�33@J�H@   ?�(�?^�R=�G�            =u=�\)>W
=    @\��@�H@   @1G�@�@���?\(�>�G�@c�
@$z�@��@��>��H>�z�?�Q�?�G�?���?���=#�
    >�    >aG�    ?���?�{@,��?�33        @�G�@p�?�R>�{@�z�@j�H?�?�Q�        =�G�    =u=L��>��<#�
>#�
    ?�p�?   @�{@��\@�@��>#�
            ?�?G�?c�
>\    =���?E�>#�
<#�
<�>��=L��?5>��
=��
    ?�\)?0��                @�p�@���@*�H?�G�>��?#�
                <#�
<�>��>8Q�?�
=>���?Y��>���@3�
?�=qA33@�
=        @$z�?��
>��    ?�33?��    =L��    >L��=�Q�>�>8Q�<�>�
=<�>�=q>L��?Ǯ?�z�@�@(��@�@��        @�  @�  ?��?���    >�{>L��    =�\)    >�<#�
>�    @�(�@|��@�33?�p�@g�@U�A6�HAAz�A�H@-p�?�@R�\@.{        =#�
=u=#�
<#�
>\)=u>k�=��
>��=#�
@���@^�R        @��
@@          @�ff@|(�@W�?���=��
>u        <�>L��=�    >8Q�    ?=p�>.{@�  @>{?��?��@���@~�RA0(�@�@��H@1�?W
=>Ǯ@\)?�        <�    >B�\    >\>�33>aG�    ?�Q�?�@�=q@G�@G�?��
>8Q�>�@
=q?�\?��?�{?�?
=q    <�<��
=#�
=�=�\)=��
    <��
    ?0��>���?�
=?u@\)?�p�?���?�?��
?:�H=�G�<��
=#�
<�>\)<�<��
<#�
<��
<�>��>�@�(�@��H?��?
=@�  @|(�>��
=�\)=#�
=�\)?&ff?
=q?��
>��H>aG�<��
>�>\)<��
        >aG�>��
>L��<#�
    ?��?J=q@��@�G�@��@��H@*�H?�ff@�=q@|(�?�>B�\?=p�?+�<�=�Q�        =u=�Q�>��>W
=>��
>W
=?p��>�@`  @��@u�@��